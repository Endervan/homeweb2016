<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 71px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_branco left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a class="active">EMPRESA</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center">
        <div class="top25">
          <h4>NOSSA EMPRESA</h4>
        </div>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- bg_site   -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row bg_site">
      <div class="container">
        <div class="row">
          <!-- ======================================================================= -->
          <!-- TITULO   -->
          <!-- ======================================================================= -->
          <div class="col-xs-12 home_toque top30">
            <h2>A HOMEWEB</h2>
          </div>
          <!-- ======================================================================= -->
          <!-- TITULO   -->
          <!-- ======================================================================= -->

          <!-- ======================================================================= -->
          <!-- desc homeweb   -->
          <!-- ======================================================================= -->
          <div class="col-xs-8 col-xs-offset-4 top40">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
            <div class=" desc_home">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- desc homeweb   -->
          <!-- ======================================================================= -->




          <!-- ======================================================================= -->
          <!-- desc homeweb   -->
          <!-- ======================================================================= -->
          <div class="col-xs-8 top120">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
            <div class=" desc_home">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- desc homeweb   -->
          <!-- ======================================================================= -->

        </div>
      </div>


    </div>
  </div>








  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<?php require_once('./includes/js_css.php') ?>
