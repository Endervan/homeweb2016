<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  71px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_branco left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a href="<?php echo Util::caminho_projeto(); ?>/servicos" data-toggle="tooltip" data-placement="top" title="SERVICOS">SERVICOS</i></a>

          <a class="active">SERVIÇO</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--TITULO GERAL -->
  <!-- ======================================================================= -->
  <div class="col-xs-12 titulo_interno text-center">
    <div class="top25 bottom10">
      <h4><?php Util::imprime($dados_dentro[titulo]); ?></span></h4>
    </div>
    <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
  </div>
  <!-- ======================================================================= -->
  <!--TITULO GERAL -->
  <!-- ======================================================================= -->

</div>
</div>

<div class="container">
  <div class="row ">

    <!-- ======================================================================= -->
    <!-- SERVICOS GERAL -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 top50  padding0 bg_branco_produtos">


      <div class="col-xs-12 top30 pb50">
        <div class="col-xs-12">
          <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",1111, 331, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>


          <div class="top25">
            <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
          </div>


          <div class="top25">
            <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
          </div>

        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- SERVICOS GERAL -->
    <!-- ======================================================================= -->

    <div class="col-xs-12 text-center">
      <a class="btn btn_saiba_mais_produtos_dentro" href="javascript:void(0);" class="btn btn_orcamento col-xs-12 top30" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
        SOLICITE UM ORÇAMENTO
      </a>

    </div>
  </div>
</div>


<div class="top50"></div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
