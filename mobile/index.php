<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>

  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 limit 5");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                <div class="carousel-caption text-center">
                  <?php /*<h1><?php Util::imprime($imagem[titulo]); ?></h1>
                  <p><?php Util::imprime($imagem[legenda]); ?></p>*/ ?>
                  <?php if (!empty($imagem[url])) { ?>
                    <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                    <?php } ?>
                  </div>
                </div>

                <?php
                $i++;
              }
            }
            ?>


          </div>



          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>



        </div>

        <div class="paginas">
          <?php require_once('./includes/topo.php'); ?>
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- slider	-->
    <!-- ======================================================================= -->





    <!-- ======================================================================= -->
    <!-- bg_site   -->
    <!-- ======================================================================= -->
    <div class="container bg_site">
      <div class="row">
        <!-- ======================================================================= -->
        <!-- TITULO   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 home_toque top30">
          <h2>A HOMEWEB</h2>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO   -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- desc homeweb   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top20">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
          <div class=" desc_home">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- desc homeweb   -->
        <!-- ======================================================================= -->




        <!-- ======================================================================= -->
        <!-- desc homeweb   -->
        <!-- ======================================================================= -->
        <div class="col-xs-offset-3 col-xs-9 top40">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
          <div class=" desc_home1">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- desc homeweb   -->
        <!-- ======================================================================= -->
        <div class="clearfix"></div>

        <!-- ======================================================================= -->
        <!-- TITULO   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 servico_toque top40">
          <h2>NOSSOS SERVIÇOS</h2>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO   -->
        <!-- ======================================================================= -->


        <?php
        $id_servicos = "meu_modal";
        ?>
        <!-- ======================================================================= -->
        <!-- PRODUTOS SERVICOS   -->
        <!-- ======================================================================= -->
        <?php
        $result = $obj_site->select("tb_servicos", "limit 2");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while($row = mysql_fetch_array($result)){

            ?>
            <div class="col-xs-12 top20 servicos_home">

              <div class="thumbnail">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 500, 245, array("class"=>"top5 input100", "alt"=>"$row[titulo]") ) ?>

                <div class="caption">
                  <h1 class="text-center"><?php Util::imprime($row[titulo]) ?></h1>

                  <p><?php Util::imprime($row[descricao], 300) ?></p>
                  <div class="top15 lato_black">
                    <a class="btn btn_saiba_mais_produtos right10" data-toggle="modal"  href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php echo Util::imprime($row[url_amigavel]) ?>" title="saiba mais" >
                      SAIBA MAIS
                    </a>

                    <a class="btn btn_saiba_mais_produtos" href="javascript:void(0);" class="btn btn_orcamento col-xs-12 top30" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                      SOLICITE UM ORÇAMENTO
                    </a>
                  </div>

                </div>
              </div>
            </div>
            <?php
            if ($i == 1) {
              echo '<div class="clearfix"></div>';

            }else{
              $i++;
            }

          }
        }
        ?>
        <!-- ======================================================================= -->
        <!-- PRODUTOS SERVICOS   -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- TITULO CLIENTES   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 cliente_toque top50">
          <h2>NOSSOS CLIENTES</h2>

          <div class="top10">
            <?php require_once('./includes/slider_clientes.php'); ?>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO CLIENTES   -->
        <!-- ======================================================================= -->



        <!-- ======================================================================= -->
        <!-- TITULO DEPOIMENTOS   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 text-center depoimentos">
          <h2>O QUE NOSSOS CLIENTES DIZEM</h2>
        </div>


        <?php
        $result = $obj_site->select("tb_depoimentos", "order by rand() limit 1");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while($row = mysql_fetch_array($result)){

            ?>

            <div class="col-xs-12 ">
              <i class="fa fa-quote-left"></i>
              <div class="top10 lato_black">
                <h3><?php Util::imprime($row[titulo]) ?></h3>
              </div>
              <div class="depoimento_home top10 bottom10">
                <p><?php Util::imprime($row[descricao], 300) ?></p>
              </div>
              <i class="fa fa-quote-right pull-right"></i>


              <?php
              if ($i == 1) {
                echo '<div class="clearfix"></div>';
              }else{
                $i++;
              }

            }
          }
          ?>

        </div>
        <!-- ======================================================================= -->
        <!-- TITULO DEPOIMENTOS   -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- FORMULARIO ORCAMENTO -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 formulario_toque top40">
          <h2>SOLICITE UM ORÇAMENTO</h2>

          <!-- ======================================================================= -->
          <!-- CONTATOS HOME -->
          <!-- ======================================================================= -->
          <div class="col-xs-12">
            <div class="top15">
              <p>TELEFONE : <span></i><?php Util::imprime($config[ddd1]) ?><?php Util::imprime($config[telefone1]) ?></span></p>
            </div>
            <div class="top15 bottom20">
              <p>E-MAIL : <span></i><?php Util::imprime($config[email_copia]) ?></span></p>
            </div>


          </div>

          <!-- ======================================================================= -->
          <!-- CONTATOS HOME -->
          <!-- ======================================================================= -->
        </div>




        <!-- ======================================================================= -->
        <!-- FORMULARIO ORCAMENTO -->
        <!-- ======================================================================= -->
        <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">

          <div class="top10 col-xs-12 top40">
            <p>  VAMOS CONVERSAR ?  </p>
          </div>

          <div class=" fundo_formulario">
            <div class="col-xs-12 top10">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="Qual é o seu nome?">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-12 top10">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="Qual é o seu e-mail?">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>


            <div class="col-xs-12 top10">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="Qual é o seu telefone?">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-12 top10">
              <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="25" rows="8" class="form-control fundo-form1 input100" placeholder="Fale um pouco sobre o projeto que deseja desenvolver,
                quando mais detalhes melhor."></textarea>
                <span class="fa fa-pencil form-control-feedback top15"></span>
              </div>
            </div>


            <div class="col-xs-12 top10">
              <div class="bottom25">
                <button type="submit" class="btn btn_saiba_mais_produtos lato_black" name="btn_contato">
                  ENVIAR ORÇAMENTO
                </button>
              </div>
            </div>

          </form>
        </div>
        <!-- ======================================================================= -->
        <!-- FORMULARIO ORCAMENTO -->
        <!-- ======================================================================= -->

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- bg_site   -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- modal    -->
    <!-- ======================================================================= -->
    <?php include('./includes/modal.php') ?>
    <!-- ======================================================================= -->
    <!-- modal    -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->


  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>



  <?php
  //  VERIFICO SE E PARA ENVIAR O EMAIL
  if(isset($_POST[nome]))
  {
    $texto_mensagem = "
    Nome: ".($_POST[nome])." <br />
    Localidade: ".($_POST[localidade])." <br />
    Telefone: ".($_POST[telefone])." <br />
    Email: ".($_POST[email])." <br />
    Mensagem: <br />
    ".(nl2br($_POST[mensagem]))."
    ";


    Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

    Util::alert_bootstrap("Obrigado por entrar em contato.");
    unset($_POST);
  }


  ?>



  <script>
  $(document).ready(function() {
    $('.FormContatos').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'fa fa-check',
        invalid: 'fa fa-remove',
        validating: 'fa fa-refresh'
      },
      fields: {
        nome: {
          validators: {
            notEmpty: {
              message: 'Insira seu nome.'
            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'Informe um email.'
            },
            emailAddress: {
              message: 'Esse endereço de email não é válido'
            }
          }
        },
        telefone: {
          validators: {
            notEmpty: {
              message: 'Por favor informe seu numero!.'
            },
            phone: {
              country: 'BR',
              message: 'Informe um telefone válido.'
            }
          },
        },
        assunto: {
          validators: {
            notEmpty: {

            }
          }
        }
      }
    });
  });
  </script>
