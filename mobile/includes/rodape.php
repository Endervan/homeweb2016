<div class="container bg-fundo-preto">
	<div class="row ">
		<div class=" col-xs-8 text-right">
			<p>© Copyright HOMEWEBBRASIL</p>
		</div>
		<div class="col-xs-4">
			<a href="http://www.homewebbrasil.com.br" class="pull-right top5" target="_blank" >
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-homeweb.png" alt="" height="40">
			</a>

			<?php if ($config[google_plus] != "") { ?>
				<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" class="pull-right right20">
					<p><i class="fa fa-google-plus"></i></p>
				</a>
				<?php } ?>

			</div>
		</div>

	</div>


<div class="whatsappFixed">
	
	<a href="https://api.whatsapp.com/send?phone=55<?php echo Util::trata_numero_whatsapp($config[ddd2].$config[telefone2]); ?>&text=Olá,%20gostaria%20de%20solicitar%20um%20orçamento.">
		<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/whatsapp.png" alt="">
	</a>
</div>