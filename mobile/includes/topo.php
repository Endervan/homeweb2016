<div class="container">
  <div class="row">

    <!--  ==============================================================  -->
    <!-- logo -->
    <!--  ==============================================================  -->
    <div class="col-xs-4 top5 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
      </a>
    </div>
    <!--  ==============================================================  -->
    <!-- logo -->
    <!--  ==============================================================  -->

    <div class="col-xs-8 top15">
      <!-- contatos topo  -->
      <div class="media col-xs-12 ">
        <div class="pull-right">
          <div class="media-left lato-black">

            <h5 class="media-heading top5">	FALE CONOSCO</h5>

          </div>
          <div class="media-body">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-telefone">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
      <!-- contatos topo  -->


      <div class="col-xs-12 text-right top5">
        <!-- ======================================================================= -->
        <!-- CARRINHO  -->
        <!-- ======================================================================= -->
        <a class="btn btn_cart"  href="javascript:void(0);" data-toggle="modal" data-target="#myModal-topo-itens-orcamento">
          <i class="fa fa-shopping-cart"></i>
        </a>
        <!-- ======================================================================= -->
        <!-- CARRINHO  -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
        <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-menu">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/carrinho_topo.png" alt="">
        </a>
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->

      </div>


    </div>


  </div>
</div>
