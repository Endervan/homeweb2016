
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
<style>
.bg-interna{
	background: #EAEAEA url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<div class="container">
		<div class="row ">

			<!-- ======================================================================= -->
			<!--TITULO GERAL -->
			<!-- ======================================================================= -->
			<div class="col-xs-12 text-center">
				<div class="top25">
					<h4>NOSSOS SERVICOS</h4>
				</div>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
			</div>
			<!-- ======================================================================= -->
			<!--TITULO GERAL -->
			<!-- ======================================================================= -->

		</div>
	</div>

	<div class="container">
		<div class="row ">

			<!-- ======================================================================= -->
			<!-- PRODUTOS SERVICOS   -->
			<!-- ======================================================================= -->
			<?php
			$result = $obj_site->select("tb_servicos");
			if (mysql_num_rows($result) > 0) {
				$i = 0;
				while($row = mysql_fetch_array($result)){

					?>
					<div class="col-xs-12 top40 servicos_home">

						<div class="thumbnail">
							<a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

								<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 500, 245, array("class"=>"top5 input100", "alt"=>"$row[titulo]") ) ?>
							</a>
							<div class="caption">
								<h1 class="text-center"><?php Util::imprime($row[titulo]) ?></h1>

								<p><?php Util::imprime($row[descricao], 300) ?></p>
								<div class="top15 lato_black">
									<a class="btn btn_saiba_mais_produtos" href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
										SAIBA MAIS
									</a>

									<a class="btn btn_saiba_mais_produtos" href="javascript:void(0);" class="btn btn_orcamento col-xs-12 top30" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
										SOLICITE UM ORÇAMENTO
									</a>
								</div>

							</div>
						</div>
					</div>
					<?php
					if ($i == 1) {
						echo '<div class="clearfix"></div>';

					}else{
						$i++;
					}

				}
			}
			?>
			<!-- ======================================================================= -->
			<!-- PRODUTOS SERVICOS   -->
			<!-- ======================================================================= -->


		</div>
	</div>




	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>



<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php include('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
