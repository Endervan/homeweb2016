

<div class="container-fluid topo">
  <div class="row">

    <div class="container">
      <div class="row">
        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->
        <div class="col-xs-2 text-center top10">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="HOME">
            <img class="right25" src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
          </a>
        </div>

        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->


        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-xs-6 menu">
          <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
          <div id="navbar">
            <ul class="nav navbar-nav navbar-right lato_bold">

              <li >
                <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/">HOME
                </a>
              </li>

              <li >
                <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/empresa">A HOMEWEB
                </a>
              </li>

              <li >
                <a class="<?php if(Url::getURL( 0 ) == "servicos"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                </a>
              </li>


              <li >
                <a class="<?php if(Url::getURL( 0 ) == "clientes"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/clientes">CLIENTES
                </a>
              </li>




            </ul>

          </div><!--/.nav-collapse -->
        </div>
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->

        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-xs-2 padding0 lato_black top3">
          <div class="dropdown">
            <a class="btn btn_topo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-shopping-cart right10"></i>MEU ORÇAMENTO <i class="fa fa-angle-double-down left10"></i>
              <?php /*<span class="badge"><?php echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?></span>*/ ?>
            </a>

            <div class="dropdown-menu topo-meu-orcamento " aria-labelledby="dropdownMenu1">
              <!--  ==============================================================  -->
              <!--sessao adicionar produtos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php if(!empty($row[imagem])): ?>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      <?php endif; ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                  </div>
                  <?php
                }
              }
              ?>


              <!--  ==============================================================  -->
              <!--sessao adicionar servicos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_servicos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php if(!empty($row[imagem])): ?>
                        <img class="carrinho_servcos" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                        <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
                      <?php endif; ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                  </div>
                  <?php
                }
              }
              ?>

              <div class="col-xs-12 text-right top10 bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_topo_orcamento" >
                  ENVIAR ORÇAMENTO
                </a>
              </div>
            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->



        <!--  ==============================================================  -->
        <!-- ATENDIMENTO-->
        <!--  ==============================================================  -->
        <div class="col-xs-2  padding0">
          <a class="btn btn_atendimento">
            ATENDIMENTO :<span class="clearfix"><i class="fa fa-phone right10"></i><?php Util::imprime($config[ddd1]) ?><?php Util::imprime($config[telefone1]) ?></span>

            <?php if (!empty($config[telefone2])): ?>
              <span class="clearfix"><i class="fa fa-phone right10"></i><?php Util::imprime($config[ddd2]) ?><?php Util::imprime($config[telefone2]) ?></span>
            <?php endif ?>

          </a>
        </div>
        <!--  ==============================================================  -->
        <!-- ATENDIMENTO-->
        <!--  ==============================================================  -->




      </div>
    </div>
  </div>
</div>
