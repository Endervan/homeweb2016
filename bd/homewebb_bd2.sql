-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 12/07/2018 às 15:40
-- Versão do servidor: 5.6.39-83.1
-- Versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `homewebb_bd2`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_atuacoes`
--

CREATE TABLE `tb_atuacoes` (
  `idatuacao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_atuacoes`
--

INSERT INTO `tb_atuacoes` (`idatuacao`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `cargo`, `telefone`, `email`, `facebook`) VALUES
(150, 'Indústria Energética', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-energética', NULL, NULL, NULL, NULL, '', '', '', NULL),
(145, 'Indústria de Exploração', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-de-exploração', NULL, NULL, NULL, NULL, '', '', '', NULL),
(146, 'Indústria de Extração', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-de-extração', NULL, NULL, NULL, NULL, '', '', '', NULL),
(147, 'Setor Automotivo', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'setor-automotivo', NULL, NULL, NULL, NULL, '', '', '', NULL),
(148, 'Indústria Alimentícia', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-alimentícia', NULL, NULL, NULL, NULL, '', '', '', NULL),
(149, 'Setor Agrícola', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'setor-agrícola', NULL, NULL, NULL, NULL, '', '', '', NULL),
(152, 'Infraestrutura', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'infraestrutura', NULL, NULL, NULL, NULL, '', '', '', NULL),
(153, 'Transporte Pesado', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'transporte-pesado', NULL, NULL, NULL, NULL, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_clientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `imagem_clientes`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Banner Index 1', '0212201612473305751586.jpg', NULL, 'SIM', NULL, '1', 'banner-index-1', '/produtos', NULL, NULL),
(2, 'Banner Index 2', '0212201602153520520817.jpg', NULL, 'NAO', NULL, '1', 'banner-index-2', '', NULL, NULL),
(4, 'Banner index 4', '1511201601003537205585.jpg', NULL, 'NAO', NULL, '1', 'banner-index-4', '', NULL, NULL),
(9, 'Mobile Index 01', '0212201601172381576378.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-01', '', NULL, NULL),
(10, 'Mobile index 02', '1211201605451585470048.jpg', NULL, 'NAO', NULL, '2', 'mobile-index-02', '', NULL, NULL),
(12, 'Mobile index 04', '2110201607061179545442.jpg', NULL, 'NAO', NULL, '2', 'mobile-index-04', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '1509201610391147458581.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Galeria', '1509201603111210263799.jpg', NULL, 'galeria', 'SIM', NULL),
(3, 'Fale conosco', '1509201605121129567670.jpg', NULL, 'fale-conosco', 'SIM', NULL),
(4, 'Orçamento', '1709201611521336768324.jpg', NULL, 'orçamento', 'SIM', NULL),
(5, 'Dicas', '1609201608371218219370.jpg', NULL, 'dicas', 'SIM', NULL),
(6, 'Dicas Dentro', '1609201609011258006348.jpg', NULL, 'dicas-dentro', 'SIM', NULL),
(7, 'Produtos', '1609201603531366994020.jpg', NULL, 'produtos', 'SIM', NULL),
(8, 'Produtos Dentro', '1609201611061326440680.jpg', NULL, 'produtos-dentro', 'SIM', NULL),
(9, 'Produtos Categoria', '1609201607201394400596.jpg', NULL, 'produtos-categoria', 'SIM', NULL),
(10, 'Trabalhe Conosco', '1609201612331144794560.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(11, 'Mobile Empresa', '2109201603451189389549.jpg', NULL, 'mobile-empresa', 'SIM', NULL),
(12, 'Mobile dicas', '2109201606061248563669.jpg', NULL, 'mobile-dicas', 'SIM', NULL),
(13, 'Mobile dicas Dentro', '2109201606071211641427.jpg', NULL, 'mobile-dicas-dentro', 'SIM', NULL),
(14, 'Mobile Fale conosco', '2109201606511301789358.jpg', NULL, 'mobile-fale-conosco', 'SIM', NULL),
(15, 'Mobile Trabalhe Conosco', '2109201608171361136822.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', NULL),
(16, 'Mobile Orçamentos', '2309201601201373699690.jpg', NULL, 'mobile-orcamentos', 'SIM', NULL),
(17, 'Mobile Produtos', '2309201608461201833813.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(18, 'Mobile Produtos Dentro', '2309201609191364375615.jpg', NULL, 'mobile-produtos-dentro', 'SIM', NULL),
(19, 'Mobile Produtos Categorias', '2309201610091163497450.jpg', NULL, 'mobile-produtos-categorias', 'SIM', NULL),
(20, 'Mobile Galeria', '2309201611361400513248.jpg', NULL, 'mobile-galeria', 'SIM', NULL),
(21, 'Mobile Galeria dentro', '2309201601071299015856.jpg', NULL, 'mobile-galeria-dentro', 'SIM', NULL),
(22, 'Galeria Dentro', '2309201604321283040851.jpg', NULL, 'galeria-dentro', 'SIM', NULL),
(23, 'Uniformes', '2809201612211279692577.jpg', NULL, 'uniformes', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'ÁREA DE SAÚDE', NULL, '1809201603061141072228.png', 'SIM', NULL, 'area-de-saude', '', '', ''),
(88, 'INFORMÁTICAS', NULL, '1909201612121205556145.png', 'SIM', NULL, 'informaticas', '', '', ''),
(86, 'INDÚSTRIA', NULL, '1909201611131121475249.png', 'SIM', NULL, 'industria', '', '', ''),
(87, 'CASA E HOTELARIA', NULL, '1909201612061161864368.png', 'SIM', NULL, 'casa-e-hotelaria', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_clientes`
--

CREATE TABLE `tb_clientes` (
  `idcliente` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `url`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(19, 'Alutec Esquadrias de Alumínio DF', NULL, 'http://alutecdf.com.br/', '0311201608278497293719..jpg', 'SIM', NULL, 'alutec-esquadrias-de-aluminio-df', '', '', ''),
(20, 'Gordeixos Pizzaria', NULL, 'http://gordeixos.com.br/', '0112201610475408578862..jpg', 'SIM', NULL, 'gordeixos-pizzaria', '', '', ''),
(21, 'DiskLimp Serviços', NULL, 'http://disklimpservicos.com.br/', '0112201610528248959891..jpg', 'SIM', NULL, 'disklimp-servicos', '', '', ''),
(22, 'CPC Guindastes', NULL, 'http://www.cpcguindastes.com.br/', '0112201610562877939397..jpg', 'SIM', NULL, 'cpc-guindastes', '', '', ''),
(23, 'Imunocentro Vacinas', NULL, 'http://www.imunocentro.com.br/', '0112201611016529150787..jpg', 'SIM', NULL, 'imunocentro-vacinas', '', '', ''),
(24, 'MilFogos', NULL, 'http://www.milfogos.com.br/', '0112201611074468749451..jpg', 'SIM', NULL, 'milfogos', '', '', ''),
(25, 'Heliossol Aquecedores', NULL, 'http://www.heliossol.com.br/', '0112201611114657685450..jpg', 'SIM', NULL, 'heliossol-aquecedores', '', '', ''),
(26, 'Line Materiais Elétricos', NULL, 'http://linemateriaiseletricos.com.br/', '0112201611147530411726..jpg', 'SIM', NULL, 'line-materiais-eletricos', '', '', ''),
(27, 'Líder Portas e Portões', NULL, 'http://liderportaseportoes.com.br/', '0112201611152178212859..jpg', 'SIM', NULL, 'lider-portas-e-portoes', '', '', ''),
(28, 'Aquatendas Piscinas', NULL, 'http://aquatendas.com.br/', '0112201611173140468853..jpg', 'SIM', NULL, 'aquatendas-piscinas', '', '', ''),
(29, 'Alkha Esquadrias de Alumínio', NULL, 'http://www.alkhaaluminio.com.br/', '0112201611194253305954..jpg', 'SIM', NULL, 'alkha-esquadrias-de-aluminio', '', '', ''),
(30, 'Fibrassol Piscinas de Fibra', NULL, 'http://www.piscinasbrasiliadf.com.br/', '0112201611218672233716..jpg', 'SIM', NULL, 'fibrassol-piscinas-de-fibra', '', '', ''),
(31, 'Unicom Marista', NULL, 'http://www.unicommarista.com.br/', '0112201611256110130651..jpg', 'SIM', NULL, 'unicom-marista', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Atendimento Goiânia - GO e Brasília - DF', '4101-4909', '', 'marciomas@gmail.com', '', NULL, NULL, 'homewebbrasil@gmail.com', 'https://plus.google.com/+HomewebGoi%C3%A2nia', '', '', '(62)', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'Marília - Diretora Financeira da Rajas Esquadrias', '<p>\r\n	Trabalhamos com a Homeweb desde o inicio de suas atividades como ag&ecirc;ncia, e temos uma experi&ecirc;ncia muito positiva com os sites que desenvolvemos com a ag&ecirc;ncia. Tivemos uma experi&ecirc;ncia anterior negativa, tanto com rela&ccedil;&atilde;o ao designer do site, como com o seu desempenho na conquistas de novos neg&eacute;cios, e fomos surpreendidos com o desempenho do nosso primeiro site desenvolvido pela homeweb. Atualmente estamos na nossa segunda plataforma desenvolvida pela Homeweb, e somos completamente atendidos, na nossa expectativas de neg&oacute;cios que o site gera, bem como na &oacute;tima experiencia de hospedagem de e-mails, que utilizamos da ag&ecirc;ncia.</p>', 'SIM', NULL, 'marilia--diretora-financeira-da-rajas-esquadrias', '1703201603181368911340..jpg'),
(2, 'Wagner - Sócio Proprietário da Rainha da Borracha', '<p>\r\n	Minha experi&ecirc;ncia com a Homeweb foi transformadora para a minha empresa. Vim de um hist&oacute;rico de muitos problemas com a empresa anterior que tinha desenvolvido o nosso site anterior, e hospedava os nossos e-mails. Atualmente, sou um cliente que recomento e indico constamente a Homeweb para outros empres&aacute;rios e amigos que me relatam experiencias ruins com as suas ag&ecirc;ncias. Fa&ccedil;o uma recomenda&ccedil;&atilde;o honesta, baseada na minha satisfa&ccedil;&atilde;o pessoal, e completa seguran&ccedil;a de indicar uma ag&ecirc;ncia s&eacute;ria e comprometida com os servi&ccedil;os que oferece e a qualidade dos seus produtos.</p>', 'SIM', NULL, 'wagner--socio-proprietario-da-rainha-da-borracha', '0803201607301356333056..jpg');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(1, '6 DICAS teste cadastro', '<p>\r\n	<strong>Correia falsificada pode causar danos ao motor</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Propriet&aacute;rios de ve&iacute;culos, profissionais e varejistas do setor automotivo devem ficar atentos: as autope&ccedil;as est&atilde;o em primeiro lugar no ranking de falsifica&ccedil;&otilde;es, segundo a ABCF (Associa&ccedil;&atilde;o Brasileira de Combate &agrave; Falsifica&ccedil;&atilde;o). Al&eacute;m de afetar o mercado, com preju&iacute;zos de R$ 3 bilh&otilde;es ao ano, essa pr&aacute;tica coloca vidas em risco, pois produtos pirateados n&atilde;o atendem aos padr&otilde;es de engenharia e normas t&eacute;cnicas de seguran&ccedil;a. Uma correia dentada original, por exemplo, &eacute; fabricada com alto n&iacute;vel de tecnologia embarcada e projetada para resistir &agrave;s situa&ccedil;&otilde;es de uso mais extremas do motor.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Segundo a ContiTech, a correia falsificada pode causar danos ao motor, como empenamento das v&aacute;lvulas de admiss&atilde;o e escape e desgaste no cabe&ccedil;ote. A quebra deste componente pode gerar um alto custo financeiro para consertar o motor, al&eacute;m dos dias sem carro. E, mais grave, se a correia quebrar com o ve&iacute;culo em movimento, o risco de acidente &eacute; enorme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para evitar problemas, a ContiTech, marca do Grupo Continental, um dos maiores fornecedores mundiais da ind&uacute;stria automobil&iacute;stica, elenca seis pontos que devem ser observados ao adquirir uma correia automotiva.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Conferir o n&uacute;mero de s&eacute;rie. Cada pe&ccedil;a possui um &uacute;nico e espec&iacute;fico n&uacute;mero de s&eacute;rie. O ideal &eacute; comparar com as demais pe&ccedil;as da loja ou oficina, para verificar se o n&uacute;mero &eacute; exclusivo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	2. Checar o c&oacute;digo exclusivo. As pe&ccedil;as da ContiTech possuem um c&oacute;digo chamado Data Matrix, uma esp&eacute;cie de QR Code. Com um smartphone &eacute; poss&iacute;vel fazer a leitura do c&oacute;digo. Se o celular n&atilde;o reconhecer, significa que a pe&ccedil;a &eacute; falsa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	3. Comparar os dados. Ao abrir o Data Matrix no celular, aparecer&aacute; uma sequ&ecirc;ncia de n&uacute;meros. Basta comparar os primeiros d&iacute;gitos com o n&uacute;mero de s&eacute;rie da pe&ccedil;a. Eles precisam ser iguais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	4. Verificar a embalagem. Correias dentadas originais de f&aacute;brica s&atilde;o embaladas em caixas lacradas. Nunca v&ecirc;m soltas, em embalagens pl&aacute;sticas ou presas com fita adesiva. J&aacute; as correias V e Multi V da ContiTech s&atilde;o apenas envolvidas por uma embalagem chamada luva. Fique atento!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	5. Pesquisar os pre&ccedil;os. Pe&ccedil;as baratas demais devem ser avaliadas. &Eacute; importante pesquisar pre&ccedil;os e desconfiar de produtos cujo valor esteja muito abaixo do praticado no mercado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	6. Exigir nota fiscal. Esta &eacute; outra forma de comprovar a autenticidade da pe&ccedil;a. E mais: com ela, &eacute; poss&iacute;vel reivindicar a garantia do produto em caso de defeito.</p>', '1609201609381398670329..jpg', 'SIM', NULL, '6-dicas-teste-cadastro', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(2, 'dica teste final', '<p>\r\n	++++teste final +++++++Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<div>\r\n	&nbsp;</div>', '1609201609411386919440..jpg', 'SIM', NULL, 'dica-teste-final', '', '', '', NULL),
(44, 'teste cadastro 01 dicas geral slider', '<p>\r\n	descricao teste cadastro 01 dicas geral slider</p>', '2109201602581371500960..jpg', 'SIM', NULL, 'teste-cadastro-01-dicas-geral-slider', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'NOSSA EMPRESA', '<div>\r\n	A HomeWeb Ag&ecirc;ncia Digital oferece o desenvolvimento de sites de neg&oacute;cios.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Por acreditar que os sites no momento atual deixaram de ser somente ilustrativos ou institucionais, e se tornaram geradores de neg&oacute;cios e uma extens&atilde;o de vendas de produtos e servi&ccedil;os da empresa f&iacute;sica, &nbsp;nos especializamos em produtos onde gerar neg&oacute;cios &eacute; o objetivo final.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Sites personalizados diretivos, de f&aacute;cil navega&ccedil;&atilde;o e localiza&ccedil;&atilde;o do produto/servi&ccedil;o, com acesso f&aacute;cil ao contato com o vendedor ou fornecedor.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Trabalhamos com tecnologia de desenvolvimento personalizado com layouts exclusivos, o que permite ao cliente a personaliza&ccedil;&atilde;o do produto as necessidades e caracter&iacute;sticas do seu neg&oacute;cio, em sites sob medida ou com m&oacute;dulos pr&eacute;-definidos.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A Homeweb Ag&ecirc;ncia Digital procura entender comportamentos, para que dessa forma possa desenvolver sites que ofere&ccedil;am a melhor experi&ecirc;ncia da p&aacute;gina destino aos seus usu&aacute;rios. Buscamos o moderno, mas com o cuidado de oferecer produtos que falem com todas as idades e todos os p&uacute;blicos. Utilizamos todos os recursos da tecnologia para desenvolver produtos que atendam as pessoas, que as fa&ccedil;am se sentir identificadas e percebam um produto ajustado as suas necessidades ao navegar pelo site.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Produtos exclusivos, personlizados, assertivos e diretivos para atender clientes reais, essa &eacute; a cara e o DNA dos sites assinados pela HomeWeb Ag&ecirc;ncia Digital.</div>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, '', '', '', 'nossa-empresa', NULL, NULL, NULL),
(2, 'EMPRESA HOME', '<p>\r\n	A HomeWeb Ag&ecirc;ncia Digital oferece o desenvolvimento de sites com foco em gera&ccedil;&atilde;o de neg&oacute;cios.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por acreditar que os sites no momento atual deixaram de ser somente ilustrativos ou institucionais, e se tornaram geradores de neg&oacute;cios e uma extens&atilde;o de vendas de produtos e servi&ccedil;os da empresa f&iacute;sica, &nbsp;nos especializamos em produtos onde gerar neg&oacute;cios &eacute; o objetivo final.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sites com PLATAFORMA 100% MOBILE (n&atilde;o trabalhamos com vers&atilde;o responsiva, ou adapt&aacute;vel, nossa plataforma mobile &eacute; desenvolvida separadamente e exclusivamente para a navega&ccedil;&atilde;o no ambiente mobile), AMP, diretivos, de f&aacute;cil navega&ccedil;&atilde;o e localiza&ccedil;&atilde;o do produto/servi&ccedil;o, com acesso f&aacute;cil ao contato com o vendedor ou fornecedor.</p>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, '', '', '', 'empresa-home', NULL, NULL, NULL),
(3, 'A HOMEWEB', '<div>\r\n	<p>\r\n		Somos uma ag&ecirc;ncia parceira com profundo conhecimento em desenolvimento de sites&nbsp; e e-commerce para navega&ccedil;&atilde;o em ambiente mobile.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		Atuando no gerenciamento de contas adwords desde 2009, com sites focados em gera&ccedil;&atilde;o de neg&oacute;cios e tecnologia que propocione ao usu&aacute;rioa a sua melhor experiencia de navega&ccedil;&atilde;o e compras pela internet.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		Oferecemos o desenvolvimento de sites com&nbsp; PLATAFORMA 100% MOBILE (n&atilde;o trabalhamos com vers&atilde;o responsiva, ou adapt&aacute;vel, nossa plataforma mobile &eacute; desenvolvida exclusivamente para a navega&ccedil;&atilde;o no ambiente mobile), SEO e hospedagem para websites.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', 'SIM', 0, '', '', '', 'a-homeweb', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_equipes`
--

CREATE TABLE `tb_equipes` (
  `idequipe` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_especificacoes`
--

CREATE TABLE `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_facebook`
--

CREATE TABLE `tb_facebook` (
  `idface` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_facebook`
--

INSERT INTO `tb_facebook` (`idface`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'face 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'face-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'face 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'face 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'face 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'face 5 Lorem ipsum dolor sit amet', '<p style=\"box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;\">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'face 6 Lorem ipsum dolor sit amet', '<p style=\"box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;\">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;\">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'face 7 Lorem ipsum dolor sit amet', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);\">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias`
--

CREATE TABLE `tb_galerias` (
  `idgaleria` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_galerias`
--

INSERT INTO `tb_galerias` (`idgaleria`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(36, 'GALERIA INDUSTRIAL', '2409201612211126493097.png', 'SIM', NULL, 'galeria-industrial', NULL),
(37, 'GALERIA DE ROUPAS', '2409201612231228099970.png', 'SIM', NULL, 'galeria-de-roupas', NULL),
(38, 'GALERIA COMÉRCIO GERAL', '1509201603311168460558.jpg', 'SIM', NULL, 'galeria-comercio-geral', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_geral`
--

CREATE TABLE `tb_galerias_geral` (
  `id_galeriageral` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_galerias_geral`
--

INSERT INTO `tb_galerias_geral` (`id_galeriageral`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(106, '2309201602251388789729.jpg', 'SIM', NULL, NULL, 36),
(107, '2309201602251277639821.jpg', 'SIM', NULL, NULL, 36),
(108, '2309201602251115800518.jpg', 'SIM', NULL, NULL, 36),
(110, '2309201606301136720385.jpg', 'SIM', NULL, NULL, 37);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(102, '1909201606421263389131.jpg', 'SIM', NULL, NULL, 9),
(103, '1909201606421402912692.jpg', 'SIM', NULL, NULL, 9),
(105, '1909201607011234732654.jpg', 'SIM', NULL, NULL, 9),
(106, '2909201607541186004782.jpg', 'SIM', NULL, NULL, 9),
(107, '2909201607541123865569.jpg', 'SIM', NULL, NULL, 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_servicos`
--

CREATE TABLE `tb_galerias_servicos` (
  `id_galeriaservico` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_uniformes`
--

CREATE TABLE `tb_galerias_uniformes` (
  `id_galeriauniformes` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_uniforme` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_galerias_uniformes`
--

INSERT INTO `tb_galerias_uniformes` (`id_galeriauniformes`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_uniforme`) VALUES
(111, '2809201612081274866319.jpg', 'SIM', NULL, NULL, 1),
(112, '2809201612081336658844.jpg', 'SIM', NULL, NULL, 1),
(113, '2809201612081284847276.jpg', 'SIM', NULL, NULL, 1),
(114, '2809201612081126000436.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galeria_empresa`
--

CREATE TABLE `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(1, 'EMPRESA GALERIA 01', '1509201611341137587104.jpg', 'SIM', NULL, 'empresa-galeria-01', NULL),
(2, 'EMPRESA GALERIA 02', '1509201611341137587104.jpg', 'SIM', NULL, NULL, NULL),
(21, 'EMPRESA GALERIA 03', '1509201611341137587104.jpg', 'SIM', NULL, NULL, NULL),
(22, 'EMPRESA GALERIA 04', '1509201611521224468872.jpg', 'SIM', NULL, 'empresa-galeria-04', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'Homeweb', 'c763face40f3a9bb4e53214c6e95edf5', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'NAO', 'homeweb', 'NAO'),
(3, 'Amanda', '0b5423952b7ebf18b2e7ff881250d89c', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', 'amanda', 'SIM'),
(4, 'Marcio', 'b7f5321302550636594df29eb4d02f15', 'SIM', 0, 'suporte@masmidia.com.br', 'SIM', NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'43\'', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'43\'', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'44\'', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'44\'', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'45\'', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'46\'', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'45\'', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'3\'', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'NAO\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'SIM\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'5\'', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'6\'', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'2\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'1\'', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'70\'', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'73\'', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'71\'', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'72\'', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'SIM\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'45\'', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = \'45\'', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'SIM\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = \'2\'', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'5\'', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'40\'', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'42\'', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'43\'', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'44\'', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'7\'', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'8\'', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'9\'', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'10\'', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'11\'', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'13\'', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'14\'', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'41\'', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'46\'', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1),
(489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:35:23', 1),
(490, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:48:13', 1),
(491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:16', 1),
(492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:59', 1),
(493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:27:37', 1),
(494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:28:23', 1),
(495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:33:30', 1),
(496, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:20', 1),
(497, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:27', 1),
(498, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:04:09', 1),
(499, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '13:12:53', 1),
(500, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:08:56', 1),
(501, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:10:32', 1),
(502, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:19:38', 1),
(503, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'5\'', '2016-06-07', '14:20:12', 1),
(504, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'3\'', '2016-06-07', '14:20:29', 1),
(505, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'4\'', '2016-06-07', '14:20:31', 1),
(506, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:25:43', 1),
(507, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:26:57', 1),
(508, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:31:49', 1),
(509, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:58:26', 1),
(510, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:01:49', 1),
(511, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-07', '15:02:31', 1),
(512, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:02:44', 1),
(513, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:06:22', 1),
(514, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-07', '15:06:29', 1),
(515, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-07', '15:07:17', 1),
(516, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:08:05', 1),
(517, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'2\'', '2016-06-07', '19:19:12', 1),
(518, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '19:20:53', 1),
(519, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-07', '16:48:33', 1),
(520, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '16:57:35', 1),
(521, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:13', 1),
(522, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:59', 1),
(523, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:05:10', 3),
(524, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:08:08', 3),
(525, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '12:30:55', 1),
(526, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '15:38:33', 1),
(527, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '18:39:40', 3),
(528, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '20:42:20', 3),
(529, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:40:49', 1),
(530, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:41:59', 1),
(531, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:43:56', 1),
(532, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:47:20', 1),
(533, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:07:52', 1),
(534, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:09:32', 1),
(535, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:10:41', 1),
(536, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:05', 1),
(537, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:39', 1),
(538, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:15:18', 1),
(539, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:16:56', 1),
(540, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:17:48', 1),
(541, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:54:16', 1),
(542, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:55:13', 1),
(543, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:37:40', 1),
(544, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:43:05', 1),
(545, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:46:24', 1),
(546, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '09:11:11', 1),
(547, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '11:34:48', 1),
(548, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '12:03:24', 1),
(549, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:02:56', 0),
(550, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:39:29', 0),
(551, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:40:31', 0),
(552, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:05:34', 0),
(553, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:13:26', 0),
(554, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:15:05', 0),
(555, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '21:31:04', 0),
(556, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:12:42', 1),
(557, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:23', 1),
(558, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:54', 1),
(559, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:14:21', 1),
(560, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:09:27', 0),
(561, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:10:20', 0),
(562, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:15:20', 0),
(563, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '01:11:29', 0),
(564, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:35:50', 1),
(565, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:55:00', 0),
(566, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:55:01', 1),
(567, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:59:56', 0),
(568, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:49:36', 1),
(569, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:50:07', 1),
(570, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '11:03:55', 1),
(571, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:52:10', 1),
(572, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '13:19:01', 1),
(573, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:00:30', 1),
(574, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:41:42', 1),
(575, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:30:13', 1),
(576, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:32:16', 1),
(577, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-06', '17:07:15', 1),
(578, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:50:58', 1),
(579, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:10', 1),
(580, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:22', 1),
(581, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:30', 1),
(582, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '13:56:40', 1),
(583, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '14:02:37', 1),
(584, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '17:20:46', 1),
(585, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-13', '21:33:07', 1),
(586, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '10:53:24', 1),
(587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:37:57', 1),
(588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:42:35', 1),
(589, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:46:29', 1),
(590, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:51:04', 1),
(591, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:24:55', 1),
(592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:27:05', 1),
(593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:28:29', 1),
(594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:02', 1),
(595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:29', 1),
(596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:39', 1),
(597, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:48', 1),
(598, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:40:05', 1),
(599, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '15:47:41', 1),
(600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:53:52', 1),
(601, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:42', 1),
(602, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:59', 1),
(603, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:13', 1),
(604, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:26', 1),
(605, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:41', 1),
(606, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:53', 1),
(607, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:59:08', 1),
(608, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '16:32:46', 1),
(609, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '18:56:30', 1),
(610, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:12', 1),
(611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:31', 1),
(612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:48', 1),
(613, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:00', 1),
(614, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:22', 1),
(615, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:46', 1),
(616, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:15:06', 1),
(617, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '19:17:31', 1),
(618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:30:31', 1),
(619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '08:52:29', 1),
(620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:58:06', 1),
(621, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '19:46:29', 1),
(622, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '19:47:35', 1),
(623, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'14\'', '2016-07-15', '19:59:25', 1),
(624, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'13\'', '2016-07-15', '19:59:32', 1),
(625, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'15\'', '2016-07-15', '19:59:36', 1),
(626, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'16\'', '2016-07-15', '19:59:42', 1),
(627, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'18\'', '2016-07-15', '19:59:45', 1),
(628, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'17\'', '2016-07-15', '19:59:49', 1),
(629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '20:14:55', 1),
(630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '11:00:46', 1),
(631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:28:19', 1),
(632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:37:56', 1),
(633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:38:07', 1),
(634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:05', 1),
(635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:53', 1),
(636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:11', 1),
(637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:27', 1),
(638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:45', 1),
(639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:06', 1),
(640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:29', 1),
(641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:58', 1),
(642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:02:46', 1),
(643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:04:37', 1),
(644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:12:13', 1),
(645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:15:36', 1),
(646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:06:46', 1),
(647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:49:01', 1),
(648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:54:14', 1),
(649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:55:10', 1),
(650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:00:07', 1),
(651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:08:04', 1),
(652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:15:21', 1),
(653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:16:03', 1),
(654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:19:17', 1),
(655, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:22:55', 1),
(656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:25:29', 1),
(657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '09:35:26', 1),
(658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '18:33:00', 1),
(659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '08:47:49', 1),
(660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:01:41', 1),
(661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:30:24', 1),
(662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:46:31', 1),
(663, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:41:53', 1),
(664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:42:27', 1),
(665, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:01:08', 1),
(666, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:59:31', 1),
(667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '13:07:58', 1),
(668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '19:12:55', 1),
(669, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_facebook WHERE idface = \'43\'', '2016-07-21', '19:23:48', 1),
(670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:05', 1),
(671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:28', 1),
(672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:48', 1),
(673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:08', 1),
(674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:26', 1),
(675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:41', 1),
(676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:28', 1),
(677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:36', 1),
(678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:46', 1),
(679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:57', 1),
(680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:34:45', 1),
(681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:57:51', 1),
(682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:59:26', 1),
(683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:01:05', 1),
(684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:38', 1),
(685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:55', 1),
(686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:09', 1),
(687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:26', 1),
(688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:45', 1),
(689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:06:02', 1),
(690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '18:07:45', 1),
(691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:29:36', 1),
(692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:32:50', 1),
(693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:33:06', 1),
(694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:14', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:39', 1),
(696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-29', '08:57:37', 1),
(697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '12:23:41', 1),
(698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '15:32:54', 1),
(699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:54:41', 1),
(700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:55:28', 1),
(701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '18:42:16', 1),
(702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '19:42:53', 1),
(703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:04:57', 1),
(704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:12:02', 1),
(705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-31', '14:29:17', 1),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:28', 1),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:42', 1),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:56', 1),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:52:43', 1),
(710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:15:31', 1),
(711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:21:54', 1),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '12:04:07', 1),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '15:58:27', 1),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '16:02:46', 1),
(715, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '17:35:49', 1),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:56:30', 1),
(717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:01', 1),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:12', 1),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:23', 1),
(720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '16:28:16', 1),
(721, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '17:20:26', 1),
(722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '18:37:39', 1),
(723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:25:58', 1),
(724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:26:08', 1),
(725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:36', 1),
(726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:47', 1),
(727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '09:09:03', 1),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '10:12:40', 1),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:44:59', 1),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:52:14', 1),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:12:56', 1),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:47:19', 1),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-04', '20:15:30', 1),
(734, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:29', 1),
(735, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:38', 1),
(736, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: teste1@gmail.com', 'DELETE FROM tb_equipes WHERE idequipe = \'1\'', '2016-08-25', '11:30:04', 1),
(737, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'2\'', '2016-08-25', '11:30:06', 1),
(738, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'3\'', '2016-08-25', '11:30:08', 1),
(739, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'4\'', '2016-08-25', '11:30:09', 1),
(740, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'5\'', '2016-08-25', '11:30:11', 1),
(741, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'6\'', '2016-08-25', '11:30:13', 1),
(742, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'7\'', '2016-08-25', '11:30:15', 1),
(743, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'8\'', '2016-08-25', '11:30:19', 1),
(744, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'9\'', '2016-08-25', '11:30:21', 1),
(745, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'10\'', '2016-08-25', '11:30:23', 1),
(746, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'11\'', '2016-08-25', '11:30:25', 1),
(747, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'12\'', '2016-08-25', '11:30:27', 1),
(748, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:12', 1),
(749, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:41', 1),
(750, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:55', 1),
(751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:02', 1),
(752, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:09', 1),
(753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:15', 1),
(754, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:22', 1),
(755, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:47', 1),
(756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:56', 1),
(757, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:05', 1),
(758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:37', 1),
(759, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:08', 1),
(760, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:30', 1),
(761, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'1\'', '2016-08-25', '11:36:53', 1),
(762, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'2\'', '2016-08-25', '11:36:54', 1),
(763, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'3\'', '2016-08-25', '11:36:56', 1),
(764, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'4\'', '2016-08-25', '11:36:58', 1),
(765, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'5\'', '2016-08-25', '11:37:01', 1),
(766, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'6\'', '2016-08-25', '11:37:03', 1),
(767, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'7\'', '2016-08-25', '11:37:05', 1),
(768, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'8\'', '2016-08-25', '11:37:07', 1),
(769, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'9\'', '2016-08-25', '11:37:09', 1),
(770, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'10\'', '2016-08-25', '11:37:10', 1),
(771, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'11\'', '2016-08-25', '11:37:12', 1),
(772, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'12\'', '2016-08-25', '11:37:13', 1),
(773, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'13\'', '2016-08-25', '11:37:15', 1),
(774, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'14\'', '2016-08-25', '11:37:17', 1),
(775, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'15\'', '2016-08-25', '11:37:19', 1),
(776, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'16\'', '2016-08-25', '11:37:21', 1),
(777, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'17\'', '2016-08-25', '11:37:22', 1),
(778, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'18\'', '2016-08-25', '11:37:24', 1),
(779, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-25', '11:38:15', 1),
(780, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-08-25', '11:38:17', 1),
(781, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-08-25', '11:38:19', 1),
(782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:39:33', 1),
(783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:41:06', 1),
(784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:48:51', 1),
(785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:49:46', 1),
(786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:51:10', 1),
(787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:18', 1),
(788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:50', 1),
(789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:02:47', 1),
(790, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'39\'', '2016-08-25', '12:02:52', 1),
(791, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'40\'', '2016-08-25', '12:02:55', 1),
(792, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'41\'', '2016-08-25', '12:02:57', 1),
(793, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'42\'', '2016-08-25', '12:02:59', 1),
(794, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'43\'', '2016-08-25', '12:03:01', 1),
(795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:03:41', 1),
(796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:14:52', 1),
(797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:15:10', 1),
(798, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-08-25', '14:21:41', 1),
(799, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-08-25', '14:21:45', 1),
(800, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-08-25', '14:21:48', 1),
(801, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-08-25', '14:21:50', 1),
(802, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-08-25', '14:21:52', 1),
(803, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-08-25', '14:21:54', 1),
(804, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'7\'', '2016-08-25', '14:21:56', 1),
(805, 'CADASTRO DO CLIENTE ', '', '2016-08-25', '14:22:57', 1),
(806, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'8\'', '2016-08-25', '14:23:51', 1),
(807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:32', 1),
(808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:55', 1),
(809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:17', 1),
(810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:30', 1),
(811, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'8\'', '2016-08-25', '14:45:02', 1),
(812, 'EXCLUSÃO DO LOGIN 50, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'50\'', '2016-08-25', '14:45:07', 1),
(813, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'51\'', '2016-08-25', '14:45:11', 1),
(814, 'EXCLUSÃO DO LOGIN 52, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'52\'', '2016-08-25', '14:45:15', 1),
(815, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'53\'', '2016-08-25', '14:45:18', 1),
(816, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'54\'', '2016-08-25', '14:45:20', 1),
(817, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'55\'', '2016-08-25', '14:45:25', 1),
(818, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:01', 1),
(819, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:21', 1),
(820, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:47', 1),
(821, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:52:26', 1),
(822, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:55:43', 1),
(823, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:57:09', 1),
(824, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:59:15', 1),
(825, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:19', 1),
(826, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:59', 1),
(827, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:10:30', 1),
(828, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:12:32', 1),
(829, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:14', 1),
(830, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:41', 1),
(831, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:20:23', 1),
(832, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:21:01', 1),
(833, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:41:28', 1),
(834, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:47', 1),
(835, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:58', 1),
(836, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:09', 1),
(837, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:17', 1),
(838, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:24', 1),
(839, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:32', 1),
(840, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:40', 1),
(841, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:49', 1),
(842, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:56', 1),
(843, 'EXCLUSÃO DO LOGIN 151, NOME: , Email: ', 'DELETE FROM tb_atuacoes WHERE idatuacao = \'151\'', '2016-08-26', '15:34:14', 1),
(844, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:36:16', 1),
(845, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:42:46', 1),
(846, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:05', 1),
(847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:22', 1),
(848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:33', 1),
(849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:46', 1),
(850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:16', 1),
(851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:52', 1),
(852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:45:04', 1),
(853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:46:30', 1),
(854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '18:51:26', 0),
(855, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '18:51:30', 0),
(856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:16:59', 0),
(857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:18:46', 0),
(858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:21:34', 0),
(859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:24:24', 0),
(860, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-08-27', '19:24:28', 0),
(861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:33:13', 0),
(862, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-08-27', '19:33:40', 0),
(863, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-27', '19:41:11', 0),
(864, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-08-27', '19:41:14', 0),
(865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:42:48', 0),
(866, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:43:20', 0),
(867, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '19:43:24', 0),
(868, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-08-27', '19:43:27', 0),
(869, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:47:39', 0),
(870, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-08-27', '19:47:43', 0),
(871, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:50:56', 0),
(872, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:54:50', 0),
(873, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-08-27', '19:54:56', 0),
(874, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-08-27', '19:54:58', 0),
(875, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-27', '19:55:00', 0),
(876, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:56:22', 0),
(877, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:19:06', 0),
(878, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '20:19:10', 0),
(879, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:20:55', 0),
(880, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:22:38', 0),
(881, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-08-27', '20:22:41', 0),
(882, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:29:14', 0),
(883, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:32:24', 0),
(884, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-27', '20:32:43', 0),
(885, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:34:14', 0),
(886, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:47:12', 0),
(887, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '20:47:22', 0),
(888, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:49:08', 0),
(889, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:53:49', 0),
(890, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:59:22', 0),
(891, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:01:55', 0),
(892, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:05:47', 0),
(893, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:27:16', 0),
(894, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:46:33', 0),
(895, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '11:46:28', 1),
(896, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'77\'', '2016-08-29', '11:46:37', 1),
(897, 'EXCLUSÃO DO LOGIN 78, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'78\'', '2016-08-29', '11:46:39', 1),
(898, 'EXCLUSÃO DO LOGIN 79, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'79\'', '2016-08-29', '11:46:41', 1),
(899, 'CADASTRO DO CLIENTE ', '', '2016-08-29', '11:49:11', 1),
(900, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '16:39:22', 13),
(901, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:47:03', 13),
(902, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:25', 13),
(903, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:41', 13),
(904, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:52', 13),
(905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:26:42', 1),
(906, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:28:49', 1),
(907, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:35:08', 1),
(908, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:57:02', 1),
(909, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:27:02', 1),
(910, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:28:27', 1),
(911, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:31:51', 1),
(912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:35:11', 1),
(913, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:38:10', 1),
(914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:53:56', 1),
(915, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:01:02', 1),
(916, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:01:40', 1),
(917, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:03:58', 1),
(918, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:04:19', 1),
(919, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:05:43', 1),
(920, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:09:40', 1),
(921, 'EXCLUSÃO DO LOGIN 74, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'74\'', '2016-09-13', '19:09:48', 1),
(922, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:26', 1),
(923, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:47', 1),
(924, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:15:07', 1),
(925, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:57:44', 1),
(926, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '20:01:10', 1),
(927, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:23:45', 1),
(928, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:24:20', 1),
(929, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:27:15', 1),
(930, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:33:52', 1),
(931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '10:39:26', 1),
(932, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:34:16', 1),
(933, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '11:46:08', 1),
(934, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:52:01', 1),
(935, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:07:45', 1),
(936, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:04', 1),
(937, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:42', 1),
(938, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:58', 1),
(939, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:11:57', 1),
(940, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:29:28', 1),
(941, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:29:48', 1),
(942, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:30:16', 1),
(943, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:31:19', 1),
(944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '17:12:33', 1),
(945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '19:50:15', 1),
(946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '08:37:22', 1),
(947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:01:49', 1),
(948, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:38:31', 1),
(949, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'37\'', '2016-09-16', '09:38:38', 1),
(950, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'38\'', '2016-09-16', '09:38:41', 1),
(951, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:41:49', 1),
(952, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:26', 1),
(953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:41', 1),
(954, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:07:02', 1),
(955, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:54:08', 1),
(956, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:02:52', 1),
(957, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:03:05', 1),
(958, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:33:32', 1),
(959, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '15:53:42', 1),
(960, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '19:20:41', 1),
(961, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '10:48:09', 1),
(962, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:49:25', 1),
(963, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:52:15', 1),
(964, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:08', 1),
(965, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:47', 1),
(966, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'1\'', '2016-09-17', '14:48:21', 1),
(967, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = \'NAO\' WHERE idunidade = \'2\'', '2016-09-17', '14:48:29', 1),
(968, 'DESATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = \'NAO\' WHERE idunidade = \'1\'', '2016-09-17', '14:48:34', 1),
(969, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '14:50:07', 1),
(970, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'2\'', '2016-09-17', '14:51:18', 1),
(971, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'1\'', '2016-09-17', '14:51:21', 1),
(972, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:27:48', 1),
(973, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:28:34', 1),
(974, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '17:30:06', 1),
(975, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:13', 1),
(976, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:49', 1),
(977, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:11:03', 1),
(978, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:14:08', 1),
(979, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:11', 1),
(980, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:30', 1),
(981, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:48', 1),
(982, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:55:08', 1),
(983, 'CADASTRO DO CLIENTE ', '', '2016-09-18', '11:09:45', 1),
(984, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:43:17', 1),
(985, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:44:24', 1),
(986, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:51', 1),
(987, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:58', 1),
(988, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:07', 1),
(989, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:13', 1),
(990, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '15:06:09', 1),
(991, 'EXCLUSÃO DO LOGIN 85, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'85\'', '2016-09-19', '11:05:44', 1),
(992, 'EXCLUSÃO DO LOGIN 76, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'76\'', '2016-09-19', '11:05:46', 1),
(993, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'83\'', '2016-09-19', '11:05:49', 1),
(994, 'EXCLUSÃO DO LOGIN 84, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'84\'', '2016-09-19', '11:05:52', 1),
(995, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:07:45', 1),
(996, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:13:50', 1),
(997, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:14:46', 1),
(998, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:15:35', 1),
(999, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:17:05', 1),
(1000, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:21:57', 1),
(1001, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:03', 1),
(1002, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:09', 1),
(1003, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:15', 1),
(1004, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:20', 1),
(1005, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:27:28', 1),
(1006, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:28:25', 1),
(1007, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:06:56', 1),
(1008, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:07:31', 1),
(1009, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:10:36', 1),
(1010, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:26', 1),
(1011, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:53', 1),
(1012, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '13:55:28', 1),
(1013, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:36:48', 1),
(1014, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:08', 1),
(1015, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:15', 1),
(1016, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:01', 1),
(1017, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:07', 1),
(1018, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:44:41', 1),
(1019, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:17', 1),
(1020, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:45', 1),
(1021, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:25', 1),
(1022, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:35', 1),
(1023, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:52', 1),
(1024, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:02:04', 1),
(1025, 'CADASTRO DO CLIENTE ', '', '2016-09-21', '14:58:35', 1),
(1026, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '15:45:26', 1),
(1027, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:06:57', 1),
(1028, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:07:12', 1),
(1029, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:51:52', 1),
(1030, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '20:17:45', 1),
(1031, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '08:46:52', 1),
(1032, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '09:19:31', 1),
(1033, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '10:09:37', 1),
(1034, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '11:36:44', 1),
(1035, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:23', 1),
(1036, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:40', 1),
(1037, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:04', 1),
(1038, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'39\'', '2016-09-23', '12:49:16', 1),
(1039, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:42', 1),
(1040, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:07:13', 1),
(1041, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:20:03', 1),
(1042, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'40\'', '2016-09-23', '14:07:52', 1),
(1043, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'41\'', '2016-09-23', '14:07:56', 1),
(1044, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '16:32:57', 1),
(1045, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:21:42', 1),
(1046, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:22:23', 1),
(1047, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:23:20', 1),
(1048, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '17:38:06', 1),
(1049, 'CADASTRO DO CLIENTE ', '', '2016-09-24', '17:39:30', 1),
(1050, 'CADASTRO DO CLIENTE ', '', '2016-09-24', '17:40:29', 1),
(1051, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '17:41:40', 1),
(1052, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '12:21:33', 1),
(1053, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '09:27:23', 1),
(1054, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '09:35:00', 1),
(1055, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '09:50:46', 1),
(1056, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '09:50:57', 1),
(1057, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '12:05:39', 1),
(1058, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '15:49:43', 1),
(1059, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '15:49:59', 1),
(1060, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '15:50:12', 1),
(1061, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '16:23:38', 1),
(1062, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '16:25:19', 1),
(1063, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '16:25:43', 1),
(1064, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '16:25:57', 1),
(1065, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-13', '16:26:14', 1),
(1066, 'CADASTRO DO CLIENTE ', '', '2016-10-13', '16:27:12', 1),
(1067, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-14', '12:41:06', 1),
(1068, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-14', '12:44:07', 1),
(1069, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-21', '19:03:53', 1),
(1070, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-21', '19:04:31', 1),
(1071, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-21', '19:06:08', 1),
(1072, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-21', '19:06:24', 1),
(1073, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:51:30', 1),
(1074, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:58:15', 1),
(1075, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:11:50', 1),
(1076, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:12:17', 1),
(1077, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:18:04', 1),
(1078, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:18:26', 1),
(1079, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:20:18', 1),
(1080, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:21:18', 1),
(1081, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:24:24', 1),
(1082, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:25:40', 1),
(1083, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:27:18', 1),
(1084, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:30:18', 1),
(1085, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:33:31', 1),
(1086, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:45:33', 1),
(1087, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-11-03', '20:46:23', 1),
(1088, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:47:27', 1),
(1089, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-11-03', '20:47:31', 1),
(1090, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:48:50', 1),
(1091, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-11-03', '20:49:57', 1),
(1092, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '20:55:03', 1),
(1093, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-11-03', '20:55:06', 1),
(1094, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '21:10:46', 1),
(1095, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-11-03', '21:11:17', 1),
(1096, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '21:15:12', 1),
(1097, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-11-03', '21:15:19', 1),
(1098, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-11-03', '21:16:32', 1),
(1099, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '17:09:04', 3),
(1100, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '17:18:10', 3),
(1101, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '17:43:27', 3),
(1102, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-11-12', '17:43:31', 3),
(1103, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-11-12', '17:43:59', 3),
(1104, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '17:45:39', 3),
(1105, 'DESATIVOU O LOGIN 10', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'10\'', '2016-11-12', '17:46:16', 3),
(1106, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'1\'', '2016-11-12', '17:46:23', 3),
(1107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '21:08:50', 3),
(1108, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'1\'', '2016-11-12', '21:08:54', 3),
(1109, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '21:14:54', 3),
(1110, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-11-12', '21:15:46', 3),
(1111, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-11-12', '21:15:59', 3),
(1112, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-11-12', '21:16:02', 3),
(1113, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-12', '21:18:29', 3),
(1114, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-11-12', '21:29:34', 3),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-15', '00:46:53', 0),
(1116, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-11-15', '00:46:58', 0),
(1117, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-15', '00:48:23', 0),
(1118, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-15', '00:54:43', 0),
(1119, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-11-15', '00:56:07', 0),
(1120, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-15', '01:00:48', 0),
(1121, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-11-15', '01:00:52', 0),
(1122, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-11-15', '01:01:37', 0),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:37:24', 1),
(1124, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:38:17', 1),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:39:23', 1),
(1126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:40:47', 1),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:42:54', 1),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:44:03', 1),
(1129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:47:40', 1),
(1130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:51:41', 1),
(1131, 'DESATIVOU O LOGIN 3', 'UPDATE tb_depoimentos SET ativo = \'NAO\' WHERE iddepoimento = \'3\'', '2016-11-30', '12:51:45', 1),
(1132, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:56:51', 1),
(1133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:57:09', 1),
(1134, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-30', '12:59:58', 1),
(1135, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'7\'', '2016-11-30', '13:00:37', 1),
(1136, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-11-30', '13:01:47', 1),
(1137, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'1\'', '2016-11-30', '13:02:25', 1),
(1138, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '22:45:51', 1),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '22:47:54', 1),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '22:52:31', 1),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '22:56:27', 1),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:01:33', 1),
(1143, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:06:33', 1),
(1144, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:07:56', 1),
(1145, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:11:31', 1),
(1146, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:14:07', 1),
(1147, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:15:50', 1),
(1148, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:17:14', 1),
(1149, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:19:41', 1),
(1150, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:21:12', 1),
(1151, 'CADASTRO DO CLIENTE ', '', '2016-12-01', '23:25:21', 1),
(1152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:31:09', 1),
(1153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:34:01', 1),
(1154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:40:08', 1),
(1155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:45:10', 1),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:50:01', 1),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-01', '23:54:50', 1),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '00:22:45', 1),
(1159, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '00:40:10', 1),
(1160, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'1\'', '2016-12-02', '00:40:14', 1),
(1161, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '00:41:55', 1),
(1162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '00:47:53', 1),
(1163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '00:55:17', 1),
(1164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '00:57:22', 1),
(1165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:04:13', 1),
(1166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:06:03', 1),
(1167, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:06:35', 1),
(1168, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:17:51', 1),
(1169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:23:15', 1),
(1170, 'DESATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'12\'', '2016-12-02', '01:23:25', 1),
(1171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:27:39', 1),
(1172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:35:39', 1),
(1173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:37:47', 1),
(1174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:41:45', 1),
(1175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:45:35', 1),
(1176, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:55:40', 1),
(1177, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:58:32', 1),
(1178, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '01:59:34', 1),
(1179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '02:10:12', 1),
(1180, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-12-02', '02:10:15', 1),
(1181, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-12-02', '02:11:14', 1),
(1182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '02:15:05', 1),
(1183, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-12-02', '02:15:10', 1),
(1184, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-12-02', '02:15:39', 1),
(1185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '02:34:48', 1),
(1186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-02', '02:36:31', 1),
(1187, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'5\'', '2016-12-02', '02:46:37', 1),
(1188, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'6\'', '2016-12-02', '02:46:39', 1),
(1189, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'7\'', '2016-12-02', '02:46:43', 1),
(1190, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'8\'', '2016-12-02', '02:46:45', 1),
(1191, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-01', '13:09:59', 1),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-01', '13:11:25', 1),
(1193, 'ALTERAÇÃO DO LOGIN 1', '', '2018-03-12', '20:22:02', 1),
(1194, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: marciomas@gmail.com', 'DELETE FROM tb_logins WHERE idlogin = \'2\'', '2018-03-12', '20:22:09', 1),
(1195, 'ALTERAÇÃO DO LOGIN 3', '', '2018-03-12', '20:22:31', 1),
(1196, 'ALTERAÇÃO DO LOGIN 1', '', '2018-03-12', '20:22:48', 1),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '12:59:36', 0),
(1198, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:01:39', 3),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:01:59', 3),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:02:09', 3),
(1201, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:02:28', 3),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:02:50', 3),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:03:50', 3),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:04:14', 3),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:04:25', 3),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:04:32', 3),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:04:50', 3),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:05:15', 3),
(1209, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:22:09', 3),
(1210, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-13', '13:23:08', 3),
(1211, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:10:54', 3),
(1212, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:16:51', 3),
(1213, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'6\'', '2018-05-23', '09:17:04', 3),
(1214, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:18:11', 3),
(1215, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:18:29', 3),
(1216, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:19:00', 3),
(1217, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:21:20', 3),
(1218, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'3\'', '2018-05-23', '09:22:17', 3),
(1219, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'11\'', '2018-05-23', '09:22:36', 3),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:23:41', 3),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:24:16', 3),
(1222, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:26:16', 3),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:26:53', 3),
(1224, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:27:28', 3),
(1225, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:27:44', 3),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:28:21', 3),
(1227, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:33:33', 3),
(1228, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:38:50', 3),
(1229, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'3\'', '2018-05-23', '09:38:59', 3),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:43:20', 3),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:44:10', 3),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:44:40', 3),
(1233, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:45:06', 3),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:45:44', 3),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:46:09', 3),
(1236, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:47:18', 3),
(1237, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:47:49', 3),
(1238, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:48:21', 3),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '09:48:43', 3),
(1240, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '18:05:21', 3),
(1241, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '18:07:01', 3),
(1242, 'CADASTRO DO CLIENTE ', '', '2018-05-23', '18:10:00', 3),
(1243, 'ALTERAÇÃO DO CLIENTE ', '', '2018-05-23', '18:10:43', 3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_lojas`
--

CREATE TABLE `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_noticias`
--

CREATE TABLE `tb_noticias` (
  `idnoticia` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'SELO QUALISOL Programa de Qualificação de Fornecedores de Sistemas de Aquecimento Solar', '<p style=\"text-align: justify;\">\r\n	O QUALISOL BRASIL &eacute; o Programa de Qualifica&ccedil;&atilde;o de Fornecedores de Sistemas de Aquecimento Solar, que engloba fabricantes, revendas e instaladoras. Fruto de um conv&ecirc;nio entre a ABRAVA, o INMETRO e o PROCEL/Eletrobras, o programa tem como objetivo garantir ao consumidor qualidade dos fornecedores de sistemas de aquecimento solar, de modo a permitir:&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	A amplia&ccedil;&atilde;o do conhecimento de fornecedores em rela&ccedil;&atilde;o ao aquecimento solar;</p>\r\n<p style=\"text-align: justify;\">\r\n	A amplia&ccedil;&atilde;o da base de mercado do aquecimento solar e suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style=\"text-align: justify;\">\r\n	O aumento da qualidade das instala&ccedil;&otilde;es e conseq&uuml;ente satisfa&ccedil;&atilde;o do consumidor final;</p>\r\n<p style=\"text-align: justify;\">\r\n	Uma melhor e mais duradoura reputa&ccedil;&atilde;o e confian&ccedil;a em sistemas de aquecimento solar nas suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style=\"text-align: justify;\">\r\n	Um crescente interesse e habilidade dos fornecedores na prospec&ccedil;&atilde;o de novos clientes e est&iacute;mulo ao surgimento de novos empreendedores.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Assim como em diversos pa&iacute;ses do mundo, no Brasil, as revendas e instaladores representam uma posi&ccedil;&atilde;o estrat&eacute;gica com rela&ccedil;&atilde;o &agrave; difus&atilde;o do aquecimento solar. Na maioria das vezes est&atilde;o em contato direto com o consumidor no momento de decis&atilde;o de compra e instala&ccedil;&atilde;o e algumas vezes tamb&eacute;m planejam e entregam os equipamentos e s&atilde;o respons&aacute;veis diretos por garantir uma instala&ccedil;&atilde;o qualificada com funcionamento, durabilidade e est&eacute;tica assegurados e comprovados. Al&eacute;m das revendas e instaladoras, as empresas fabricantes de equipamentos solares tamb&eacute;m realizam instala&ccedil;&otilde;es e contratos diretos com consumidores finais e assumem a responsabilidade por todo o processo desde a venda at&eacute; a instala&ccedil;&atilde;o e p&oacute;s-venda.&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Portanto, a qualidade dos sistemas de aquecimento solar depende diretamente de:</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	bons produtos, etiquetados;</p>\r\n<p style=\"text-align: justify;\">\r\n	bons projetistas e revendas, qualificadas;</p>\r\n<p style=\"text-align: justify;\">\r\n	bons instaladores, qualificados;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Heliossol Energia Para a Vida!</p>', '0206201605386174124988..jpg', 'SIM', NULL, 'selo-qualisol-programa-de-qualificacao-de-fornecedores-de-sistemas-de-aquecimento-solar', '', '', '', NULL),
(50, 'ENERGIA RENOVÁVEL NO BRASIL - NÃO BASTA SOMENTE GERAR, TEMOS QUE SABER USÁ-LA', '<p>\r\n	A gera&ccedil;&atilde;o de energia renov&aacute;vel mundial apresentar&aacute; forte crescimento nos pr&oacute;ximos anos, com expectativa de crescimento de 12,7% no per&iacute;odo entre 2010 a 2013 segundo a International Energy Agency - IEA. As raz&otilde;es principais dessa previs&atilde;o s&atilde;o as metas de redu&ccedil;&atilde;o de emiss&otilde;es de CO2 e mudan&ccedil;as clim&aacute;ticas; melhorias tecnol&oacute;gicas favorecendo novas alternativas; aumento na demanda de energia; ambiente regulat&oacute;rio mais favor&aacute;vel; e incentivos governamentais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Enquanto os benef&iacute;cios de desenvolver-se uma agenda s&oacute;lida para consolida&ccedil;&atilde;o da energia renov&aacute;vel no Brasil s&atilde;o evidentes, &eacute; preciso atentar-se para os riscos associados &agrave; segrega&ccedil;&atilde;o do tema de energias renov&aacute;veis do panorama geral da agenda energ&eacute;tica no Brasil, atualmente levada pela ANP (Ag&ecirc;ncia Nacional do Petr&oacute;leo, G&aacute;s Natural e Biocombust&iacute;veis) e ANEEL (Ag&ecirc;ncia Nacional de Energia El&eacute;trica. &Eacute; preciso existir um incentivo maior &agrave;s pol&iacute;ticas p&uacute;blicas que tornem economicamente vi&aacute;veis melhorias de projetos n&atilde;o s&oacute; voltadas para reduzir os gastos com energia el&eacute;trica, aumentando a efici&ecirc;ncia energ&eacute;tica dos processos, como tamb&eacute;m que possibilitem o uso sustent&aacute;vel dos recursos naturais, diz Ricardo Antonio do Esp&iacute;rito Santo Gomes, consultor em efici&ecirc;ncia energ&eacute;tica do CTE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para Gomes &eacute; preciso pensar mais na maneira como usamos a energia, n&atilde;o somente na maneira como a geramos: N&atilde;o adianta gerarmos muita energia de forma eficiente se aqui na ponta usamos chuveiro el&eacute;trico, ao inv&eacute;s de aquecedor solar t&eacute;rmico por exemplo. Portanto, a gera&ccedil;&atilde;o distribu&iacute;da &eacute; a maneira mais eficiente de se garantir um crescimento sustent&aacute;vel para a sociedade, diminuindo perdas em transmiss&atilde;o, diminuindo o impacto ambiental de grandes centrais geradoras de energia e produzindo, localmente, as utilidades de que o cliente final precisa, como energia el&eacute;trica, vapor, &aacute;gua quente e &aacute;gua gelada, garantindo que um combust&iacute;vel se transforme ao m&aacute;ximo poss&iacute;vel, em outras formas de energia, causando menor impacto poss&iacute;vel e garantindo o n&iacute;vel de conforto exigido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele acrescenta que na pauta de pesquisas e desenvolvimento est&atilde;o os sistemas que possuem o atrativo de manterem altas taxas de efici&ecirc;ncia energ&eacute;tica, baixa emiss&atilde;o de poluentes e de CO2 e redu&ccedil;&atilde;o de custos de transmiss&atilde;o.</p>\r\n<div>\r\n	&nbsp;</div>', '0206201605489606782930..jpg', 'SIM', NULL, 'energia-renovavel-no-brasil--nao-basta-somente-gerar-temos-que-saber-usala', '', '', '', NULL),
(48, 'BANHO AQUECIDO ATRAVÉS DE AQUECIMENTO SOLAR', '<p>\r\n	Apesar de abundante, a energia solar como energia t&eacute;rmica &eacute; pouco aproveitada no Brasil, com isso o banho quente brasileiro continua, em 67% dos lares brasileiros, a utilizar o chuveiro el&eacute;trico que consome 8% da eletricidade produzida ao inv&eacute;s do aquecedor solar, segundo dados do Instituto Vitae Civillis &quot;Um Banho de Sol para o Brasil&quot;, de D&eacute;lcio Rodrigues e Roberto Matajs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esses n&uacute;meros contrastam com a capacidade que o territ&oacute;rio brasileiro tem de produzir energia solar: O potencial de gera&ccedil;&atilde;o &eacute; equivalente a 15 trilh&otilde;es de MW/h, correspondente a 50 mil vezes o consumo nacional de eletricidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O projeto Cidade Solares, parceria entre o Vitae Civilis e a DASOL/ABRAVA*, foi criado para discutir, propor e acompanhar a tramita&ccedil;&atilde;o e entrada em vigor de leis que incentivam ou obrigam o uso de sistemas solares de aquecimento de &aacute;gua nas cidades e estados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A id&eacute;ia &eacute; debater e difundir conhecimento sobre o assunto, al&eacute;m de analisar as possibilidades de implanta&ccedil;&atilde;o de projetos nas cidades. Ao ganhar o t&iacute;tulo de &quot;Solar&quot;, uma cidade servir&aacute; de exemplo para outras, com a difus&atilde;o do tema e das tecnologias na pr&aacute;tica. No site, o projeto lista v&aacute;rias iniciativas de cidades que implantaram com sucesso sistemas solares, como Freiburg- na Alemanha, Graz- na &Aacute;ustria, Portland- nos EUA e Oxford- na Inglaterra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* DASOL/ ABRAVA: Departamento Nacional de Aquecimento Solar da Associa&ccedil;&atilde;o Brasileira de Refrigera&ccedil;&atilde;o, Ar-condicionado, Ventila&ccedil;&atilde;o e Aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte: Revista Super Interessante - Editora Abril</p>', '0206201605411524312164..jpg', 'SIM', NULL, 'banho-aquecido-atraves-de-aquecimento-solar', '', '', '', NULL),
(47, 'COMO FUNCIONA UM SISTEMA DE AQUECIMENTO SOLAR DE ÁGUA? VOCÊ SABE?', '<p>\r\n	A mesma energia solar que ilumina e aquece o planeta pode ser usada para esquentar a &aacute;gua dos nossos banhos, acenderem l&acirc;mpadas ou energizar as tomadas de casa. O sol &eacute; uma fonte inesgot&aacute;vel de energia e, quando falamos em sustentabilidade, em economia de recursos e de &aacute;gua, em economia de energia e redu&ccedil;&atilde;o da emiss&atilde;o de g&aacute;s carb&ocirc;nico na atmosfera, nada mais natural do que pensarmos numa maneira mais eficiente de utiliza&ccedil;&atilde;o da energia solar. Esta energia &eacute; totalmente limpa e, principalmente no Brasil, onde temos uma enorme incid&ecirc;ncia solar, os sistemas para o aproveitamento da energia do sol s&atilde;o muito eficientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para ser utilizada, a energia solar deve ser transformada e, para isto, h&aacute; duas maneiras principais de realizar essa transforma&ccedil;&atilde;o: Os pain&eacute;is fotovoltaicos e os aquecedores solares.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os primeiros s&atilde;o respons&aacute;veis pela transforma&ccedil;&atilde;o da energia solar em energia el&eacute;trica. Com esses pain&eacute;is podemos utilizar o sol para acender as l&acirc;mpadas das nossas casas ou para ligar uma televis&atilde;o. A segunda forma &eacute; com o uso de aquecedores solares, que utiliza a energia solar para aquecer a &aacute;gua que ser&aacute; direcionada aos chuveiros ou piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como funciona um sistema de aquecimento solar?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Basicamente este sistema &eacute; composto por dois elementos: Os coletores solares (pain&eacute;is de capta&ccedil;&atilde;o, que vemos freq&uuml;entemente nos telhados das casas) e o reservat&oacute;rio de &aacute;gua quente, tamb&eacute;m chamado de boiler.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os coletores s&atilde;o formados por uma placa de vidro que isola do ambiente externo aletas de cobre ou alum&iacute;nio pintadas com tintas especiais na cor escura para que absorvam o m&aacute;ximo da radia&ccedil;&atilde;o. Ao absorver a radia&ccedil;&atilde;o, estas aletas deixam o calor passar para tubos em forma de serpentina geralmente feitos de cobre. Dentro desses tubos passa &aacute;gua, que &eacute; aquecida antes de ser levada para o reservat&oacute;rio de &aacute;gua quente. Estas placas coletoras podem ser dispostas sobre telhados e lajes e a quantidade de placas instaladas varia conforme o tamanho do reservat&oacute;rio, o n&iacute;vel de insola&ccedil;&atilde;o da regi&atilde;o e as condi&ccedil;&otilde;es de instala&ccedil;&atilde;o. No hemisf&eacute;rio sul, normalmente as placas ficam inclinadas para a dire&ccedil;&atilde;o norte para receber a maior quantidade poss&iacute;vel de radia&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios s&atilde;o cilindros de alum&iacute;nio, inox e polipropileno com isolantes t&eacute;rmicos que mant&eacute;m pelo maior tempo poss&iacute;vel a &aacute;gua aquecida. Uma caixa de &aacute;gua fria abastece o sistema para que o boiler fique sempre cheio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios devem ser instalados o mais pr&oacute;ximo poss&iacute;vel das placas coletoras (para evitar perda de efici&ecirc;ncia do sistema), de prefer&ecirc;ncia devem estar sob o telhado (para evitar a perda de calor para a atmosfera) e em n&iacute;vel um pouco elevado. Dessa forma, consegue-se o efeito chamado de termossif&atilde;o, ou seja, conforme a &aacute;gua dos coletores vai esquentando, ela torna-se menos densa e vai sendo empurrada pela &aacute;gua fria. Assim ela sobe e chega naturalmente ao boiler, sem a necessidade de bombeamento. Em casos espec&iacute;ficos, em que o reservat&oacute;rio n&atilde;o possa ser instalado acima das placas coletoras, podem-se utilizar bombas para promover a circula&ccedil;&atilde;o da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E nos dias nublados, chuvosos ou &agrave; noite?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apesar de poderem ser instalados de forma independente, os aquecedores solares normalmente trabalham com um sistema auxiliar de aquecimento da &aacute;gua. Este sistema pode ser el&eacute;trico ou a g&aacute;s, e j&aacute; vem de f&aacute;brica. Quando houver uma seq&uuml;&ecirc;ncia de dias nublados ou chuvosos em que a energia gerada pelo aquecedor solar n&atilde;o seja suficiente para esquentar toda a &aacute;gua necess&aacute;ria para o consumo di&aacute;rio, um aquecedor el&eacute;trico ou a g&aacute;s &eacute; acionado, gerando &aacute;gua quente para as pias e chuveiros. O mesmo fato acontece &agrave; noite quando n&atilde;o houver mais &aacute;gua aquecida no reservat&oacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Num pa&iacute;s tropical como o nosso, na grande maioria dos dias a &aacute;gua ser&aacute; aquecida com a energia solar e, portanto, os sistemas auxiliares ficar&atilde;o desligados a maior parte do tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena economicamente?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O custo do sistema &eacute; compensado pela economia mensal na conta de energia el&eacute;trica. O Aquecedor Solar Heliotek reduz at&eacute; 80% dos custos de energia com aquecimento e em poucos anos o sistema se paga, gerando lucro ao longo de sua vida &uacute;til? 20 anos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os aquecedores solares podem ser instalados em novas obras ou durante reformas. Ao construir ou reformar, estude esta op&ccedil;&atilde;o, que &eacute; boa para o planeta e pode ser &oacute;tima para a sua conta de energia.</p>', '0206201605446692289010..jpg', 'SIM', NULL, 'como-funciona-um-sistema-de-aquecimento-solar-de-agua-voce-sabe', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_parceiros`
--

CREATE TABLE `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(3, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_portifolios`
--

CREATE TABLE `tb_portifolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_portifolios`
--

INSERT INTO `tb_portifolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`) VALUES
(9, 'teste cadastro para galeria', '1309201605281303915101..jpg', '<p>\r\n	Para garantir o correto funcionamento e sincronismo entre o eixo do comando de v&aacute;lvulas e o virabrequim, a constru&ccedil;&atilde;o da correia sincronizadora possui cordon&eacute;is de fibra-de-vidro (alta resist&ecirc;ncia &agrave; tra&ccedil;&atilde;o), composto de borracha diferenciado (maior durabilidade) e perfis de dentes especialmente projetados para cada sistema de transmiss&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Com o aumento da pot&ecirc;ncia dos motores e sua maior temperatura de trabalho, foi desenvolvido um novo composto de borracha, o HNBR, com maior resist&ecirc;ncia em rela&ccedil;&atilde;o ao material utilizado anteriormente (Cloroprene). Por isso a Gates recomenda a utiliza&ccedil;&atilde;o de correias com composto de borracha HNBR, para ve&iacute;culos fabricados ap&oacute;s o ano de 1996.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Caracter&iacute;sticas das Correias Sincronizadoras</strong></p>\r\n<p>\r\n	&bull; Maior resist&ecirc;ncia a &oacute;leo, ao calor e ao Oz&ocirc;nio</p>\r\n<p>\r\n	&bull; Dentes moldados com precis&atilde;o</p>\r\n<p>\r\n	&bull; Tecidos resistentes &agrave; abras&atilde;o</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Aplica&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	&bull; Comando de v&aacute;lvulas</p>\r\n<p>\r\n	&bull; Eixo balanceador</p>', 75, 2, '', '', '', 'SIM', 0, 'teste-cadastro-para-galeria', '4545454545454545', NULL, NULL, NULL, NULL, '111222233333', 'SIM'),
(10, 'CALCAS', '1909201601551360653284..jpg', '<p>\r\n	TESTE CADASTRO PRODUTOS CALCA</p>', 87, 8, '', '', '', 'SIM', 0, 'calcas', NULL, NULL, NULL, NULL, NULL, '0111111', 'SIM');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos_aplicacoes`
--

CREATE TABLE `tb_produtos_aplicacoes` (
  `idprodutoaplicacao` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_produtos_aplicacoes`
--

INSERT INTO `tb_produtos_aplicacoes` (`idprodutoaplicacao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `titulo`) VALUES
(11, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 1'),
(12, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 2'),
(13, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 3'),
(14, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 4'),
(15, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 5');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(3, 16, 1, 6, 4, 1987, 2013, 'SIM', 0, ''),
(4, 16, 1, 7, 5, 1987, 2013, 'SIM', 0, ''),
(5, 16, 1, 8, 6, 1987, 2013, 'SIM', 0, ''),
(6, 16, 1, 9, 7, 1987, 2013, 'SIM', 0, ''),
(7, 15, 1, 7, 5, 2000, 2016, 'SIM', 0, ''),
(8, 15, 1, 8, 6, 2000, 2016, 'SIM', 0, ''),
(10, 2, 1, 6, 3, 1990, 2016, 'SIM', 0, ''),
(11, 4, 3, 5, 2, 1994, 2004, 'SIM', 0, ''),
(12, 3, 1, 7, 5, 1980, 2016, 'SIM', 0, ''),
(13, 17, 8, 7, 5, 2009, 2013, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerce com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce DF', 'SIM', NULL, 'index'),
(2, 'Empresa', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerce com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce', 'SIM', NULL, 'empresa'),
(3, 'Dicas', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerces com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce .', 'SIM', NULL, 'dicas'),
(5, 'Orçamentos', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerces com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce', 'SIM', NULL, 'orcamentos'),
(6, 'Galeria', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerces com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce', 'SIM', NULL, 'galeria'),
(7, 'Produtos', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerces com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce', 'SIM', NULL, 'produtos'),
(8, 'Fale Conosco', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerce com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce .', 'SIM', NULL, 'fale-conosco'),
(9, 'Trabalhe Conosco', 'HOMEWEB AGÊNCIA DIGITAL BRASILIA DF | Desenvolvimento de Sites, Criação de E-commerce', 'A Homeweb Agência Digital é uma agência especializada e exclusiva em de desenvolvimento de sites e  E-commerce com PLATAFORMA 100% MOBILE (não trabalhamos com versão responsiva, ou adaptável, nossa plataforma mobile é desenvolvida exclusivamente para a navegação no ambiente mobile),', 'Agência Digital Goiânia, Agência Digital DF, Agência Digital Brasília DF, Desenvolvimento de Site Brasília, Desenvolvimento de Site DF, Desenvolvimento de Site Brasilia DF, Criação de Sites DF, Criação de Site com Mobile DF, Site Mobile DF, Site AMP, Site Mobile, Site Responsivo, E-commerce DF, E-commerce Mobile, E-commerce Mobile DF, Criação de E-commerce DF, Desenvolvimento de E-commerce', 'SIM', NULL, 'trabalhe-conosco');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `url`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`, `imagem_principal`) VALUES
(5, 'DESENVOLVIMENTO DE SITES', '<p>\r\n	Sites com plataforma em duas vers&otilde;es para navega&ccedil;&atilde;o em PC e Mobile (web app) de f&aacute;cil navega&ccedil;&atilde;o e localiza&ccedil;&atilde;o do produto/servi&ccedil;o, com acesso f&aacute;cil ao contato com o vendedor ou fornecedor.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Trabalhamos com tecnologia de desenvolvimento 100% mobile e AMP, o que permite ao nosso cliente a disponibiliza&ccedil;&atilde;o para o usu&aacute;rio um produto atendendo as mais exigentes caracter&iacute;sticas em tecnologia de websites com m&oacute;dulos pr&eacute;-definidos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Homeweb Ag&ecirc;ncia Digital procura entender comportamentos, para que dessa forma possa desenvolver sites que ofere&ccedil;am a melhor experi&ecirc;ncia da p&aacute;gina destino aos seus usu&aacute;rios. Buscamos o moderno, mas com o cuidado de oferecer produtos que falem com todas as idades e todos os p&uacute;blicos. Utilizamos todos os recursos da tecnologia para desenvolver produtos que atendam as pessoas, que as fa&ccedil;am se sentir identificadas e percebam um produto ajustado as suas necessidades ao navegar pelo site.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Produtos exclusivos, de tecnologia pr&oacute;pria, assertivos e diretivos para atender clientes reais, essa &eacute; a cara e o DNA dos sites assinados pela HomeWeb Ag&ecirc;ncia Digital.</p>\r\n<div>\r\n	&nbsp;</div>', '0212201601592529690774..jpg', 'SIM', NULL, 'desenvolvimento-de-sites', NULL, 'Agência de Criação e Desenvolvimento de Sites e E-commerce', 'Agência de Criação e Desenvolvimento de Sites e E-commerces', 'Agência de Criação e Desenvolvimento de Sites e E-commerce', 0, '', '', '0206201611176686009934.png', '2508201602594049189843.jpg'),
(57, 'DESENVOLVIMENTO DE E-COMMERCE', '<p>\r\n	Desenvolvimento de e-commerce com plataforma em duas vers&otilde;es para navega&ccedil;&atilde;o em PC e Mobile (web app), de f&aacute;cil navega&ccedil;&atilde;o e localiza&ccedil;&atilde;o do produto/servi&ccedil;o, com acesso f&aacute;cil ao contato com o vendedor ou fornecedor.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Trabalhamos com tecnologia de desenvolvimento 100% mobile e AMP, o que permite ao nosso cliente a disponibiliza&ccedil;&atilde;o para o usu&aacute;rio um produto atendendo as mais exigentes caracter&iacute;sticas em tecnologia de websites com m&oacute;dulos pr&eacute;-definidos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Homeweb Ag&ecirc;ncia Digital procura entender comportamentos, para que dessa forma possa desenvolver sites que ofere&ccedil;am a melhor experi&ecirc;ncia da p&aacute;gina destino aos seus usu&aacute;rios. Buscamos o moderno, mas com o cuidado de oferecer produtos que falem com todas as idades e todos os p&uacute;blicos. Utilizamos todos os recursos da tecnologia para desenvolver produtos que atendam as pessoas, que as fa&ccedil;am se sentir identificadas e percebam um produto ajustado as suas necessidades ao navegar pelo site.</p>\r\n<div>\r\n	&nbsp;</div>', '2305201806092070646124..jpg', 'SIM', NULL, 'desenvolvimento-de-ecommerce', NULL, '', '', '', 0, '', '', NULL, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_subcategorias_produtos`
--

CREATE TABLE `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'COMISSÃO', 75, 'SIM', NULL, 'comissao'),
(2, 'MEDICAÇÃO', 75, 'SIM', NULL, 'medicacao'),
(3, 'ENFERMEIROS', 75, 'SIM', NULL, 'enfermeiros'),
(4, 'AUTOMOVÉIS', 86, 'SIM', NULL, 'automoveis'),
(5, 'COMERCIO EM GERAL', 86, 'SIM', NULL, 'comercio-em-geral'),
(6, 'MEDICOS', 75, 'SIM', NULL, 'medicos'),
(7, 'LOJAS', 86, 'SIM', NULL, 'lojas'),
(8, 'ROUPAS MASCULINAS', 87, 'SIM', NULL, 'roupas-masculinas'),
(9, 'ALUGUÉIS DE CADEIRAS', 87, 'SIM', NULL, 'alugueis-de-cadeiras'),
(10, 'DESENVOLVIMENTO DE SISTEMAS', 88, 'SIM', NULL, 'desenvolvimento-de-sistemas');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_tipos_veiculos`
--

CREATE TABLE `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `imagem`) VALUES
(1, 'Carros', 0, 'SIM', 'carros', '2006201608061191940846.png'),
(2, 'Motos', 0, 'SIM', 'motos', '2006201608071116286885.png'),
(3, 'Caminhões', 0, 'SIM', 'caminhoes', '2006201608071262816952.png'),
(8, 'Naúticos', 0, 'SIM', 'nauticos', '2006201608081392104809.png'),
(9, 'Nobreaks', 0, 'SIM', 'nobreaks', '2006201608091390486381.png'),
(10, 'Lâmpadas', 0, 'SIM', 'lampadas', '2006201608101362518866.png');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_unidades`
--

CREATE TABLE `tb_unidades` (
  `idunidade` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `src_place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Fazendo dump de dados para tabela `tb_unidades`
--

INSERT INTO `tb_unidades` (`idunidade`, `titulo`, `src_place`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'goiania', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d244583.7565257507!2d-49.444355989195444!3d-16.695828772934586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef6bd58d80867%3A0xef692bad20d2678e!2zR29pw6JuaWEsIEdP!5e0!3m2!1spt-BR!2sbr!4v1474129', 'SIM', NULL, 'goiania', NULL, NULL, NULL),
(2, 'Brasilia', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d244583.7565257507!2d-49.444355989195444!3d-16.695828772934586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef6bd58d80867%3A0xef692bad20d2678e!2zR29pw6JuaWEsIEdP!5e0!3m2!1spt-BR!2sbr!4v1474129', 'SIM', NULL, 'brasilia', NULL, NULL, NULL),
(3, 'são paulo', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3822.0458683248467!2d-49.28476368513354!3d-16.67458798850948!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5e02474a8cbf0ef0!2sRainha+da+Borracha!5e0!3m2!1spt-BR!2sus!4v1472135158080', 'SIM', NULL, 'são-paulo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_uniformes`
--

CREATE TABLE `tb_uniformes` (
  `iduniforme` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_uniformes`
--

INSERT INTO `tb_uniformes` (`iduniforme`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Uniformes de Saúdes', '2409201605381218447454.png', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'uniformes-de-saudes', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(8, 'Uniformes de Limpeza geral', '2409201605411332434728.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-de-limpeza-geral', NULL),
(9, 'Uniformes Cozinheiros', '2409201605401358107866.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-cozinheiros', NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  ADD PRIMARY KEY (`idatuacao`);

--
-- Índices de tabela `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Índices de tabela `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Índices de tabela `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Índices de tabela `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Índices de tabela `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`idcliente`);

--
-- Índices de tabela `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Índices de tabela `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Índices de tabela `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Índices de tabela `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Índices de tabela `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Índices de tabela `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Índices de tabela `tb_equipes`
--
ALTER TABLE `tb_equipes`
  ADD PRIMARY KEY (`idequipe`);

--
-- Índices de tabela `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Índices de tabela `tb_facebook`
--
ALTER TABLE `tb_facebook`
  ADD PRIMARY KEY (`idface`);

--
-- Índices de tabela `tb_galerias`
--
ALTER TABLE `tb_galerias`
  ADD PRIMARY KEY (`idgaleria`);

--
-- Índices de tabela `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  ADD PRIMARY KEY (`id_galeriageral`);

--
-- Índices de tabela `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Índices de tabela `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Índices de tabela `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  ADD PRIMARY KEY (`id_galeriaservico`);

--
-- Índices de tabela `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  ADD PRIMARY KEY (`id_galeriauniformes`);

--
-- Índices de tabela `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  ADD PRIMARY KEY (`idgaleriaempresa`);

--
-- Índices de tabela `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Índices de tabela `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Índices de tabela `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Índices de tabela `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Índices de tabela `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Índices de tabela `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Índices de tabela `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Índices de tabela `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  ADD PRIMARY KEY (`idprodutoaplicacao`);

--
-- Índices de tabela `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  ADD PRIMARY KEY (`idprodutosmodeloaceito`);

--
-- Índices de tabela `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Índices de tabela `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Índices de tabela `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  ADD PRIMARY KEY (`idsubcategoriaproduto`);

--
-- Índices de tabela `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  ADD PRIMARY KEY (`idtipoveiculo`);

--
-- Índices de tabela `tb_unidades`
--
ALTER TABLE `tb_unidades`
  ADD PRIMARY KEY (`idunidade`);

--
-- Índices de tabela `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  ADD PRIMARY KEY (`iduniforme`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  MODIFY `idatuacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT de tabela `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de tabela `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT de tabela `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de tabela `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de tabela `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `tb_equipes`
--
ALTER TABLE `tb_equipes`
  MODIFY `idequipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de tabela `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `tb_facebook`
--
ALTER TABLE `tb_facebook`
  MODIFY `idface` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de tabela `tb_galerias`
--
ALTER TABLE `tb_galerias`
  MODIFY `idgaleria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de tabela `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  MODIFY `id_galeriageral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT de tabela `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de tabela `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT de tabela `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  MODIFY `id_galeriaservico` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  MODIFY `id_galeriauniformes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT de tabela `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  MODIFY `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1244;

--
-- AUTO_INCREMENT de tabela `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de tabela `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de tabela `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  MODIFY `idprodutoaplicacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  MODIFY `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de tabela `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  MODIFY `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  MODIFY `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `tb_unidades`
--
ALTER TABLE `tb_unidades`
  MODIFY `idunidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de tabela `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  MODIFY `iduniforme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
